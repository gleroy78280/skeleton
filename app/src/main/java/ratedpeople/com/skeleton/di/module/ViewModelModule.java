package ratedpeople.com.skeleton.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import ratedpeople.com.skeleton.presentation.viewmodel.base.ViewModelFactory;

@Module
public abstract class ViewModelModule {

    // uncomment next lines to add a ViewModel
//    @Binds
//    @IntoMap
//    @ViewModelKey(MyViewModel.class)
//    abstract ViewModel bindMyViewModel(MyViewModel viewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
