package ratedpeople.com.skeleton.di.component;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import ratedpeople.com.skeleton.MyApplication;
import ratedpeople.com.skeleton.di.module.AndroidBindingModule;
import ratedpeople.com.skeleton.di.module.AppModule;
import ratedpeople.com.skeleton.di.module.DatabaseModule;
import ratedpeople.com.skeleton.di.module.NetModule;
import ratedpeople.com.skeleton.di.module.ViewModelModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        NetModule.class,
        ViewModelModule.class,
        AndroidBindingModule.class,
        DatabaseModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        void appModule(AppModule module);

        void netModule(NetModule module);

        AppComponent build();
    }

    void inject(MyApplication app);

}
