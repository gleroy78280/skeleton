package ratedpeople.com.skeleton.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ratedpeople.com.skeleton.presentation.activity.MainActivity;

@Module
public abstract class AndroidBindingModule {

    ////////////////////////////////////
    // Activity
    ////////////////////////////////////

    @ContributesAndroidInjector
    abstract MainActivity MainActivity();

    ////////////////////////////////////
    // Fragments
    ////////////////////////////////////

    // uncomment next lines to add a Fragment
//    @ContributesAndroidInjector
//    abstract MyFragment MyFragment();


}