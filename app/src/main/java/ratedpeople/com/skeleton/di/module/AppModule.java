package ratedpeople.com.skeleton.di.module;

import android.app.Application;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import ratedpeople.com.skeleton.utils.react.BaseSchedulerProvider;
import ratedpeople.com.skeleton.utils.react.SchedulerProvider;

@Module
public class AppModule {

    @NonNull
    private Application mApplication;

    public AppModule(@NonNull Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    BaseSchedulerProvider provideSchedulerProvider(SchedulerProvider schedulerProvider) {
        return schedulerProvider;
    }

}


