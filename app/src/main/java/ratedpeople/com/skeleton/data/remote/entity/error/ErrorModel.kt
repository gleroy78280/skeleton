package ratedpeople.com.skeleton.data.remote.entity.error

import java.io.Serializable

class ErrorModel(var code: String?, var message: String?) : Serializable
