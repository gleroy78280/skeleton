package ratedpeople.com.skeleton.data.remote.api.base

import ratedpeople.com.skeleton.data.remote.entity.ApiResponse
import ratedpeople.com.skeleton.data.remote.entity.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MyApi {

    @GET("/api")
    fun getUsers(@Query("results") count: String, @Query("page") page: String): Call<ApiResponse<User>>
}
