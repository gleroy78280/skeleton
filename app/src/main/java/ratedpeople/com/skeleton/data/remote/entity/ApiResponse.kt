package ratedpeople.com.skeleton.data.remote.entity

class ApiResponse<T>(val results: List<T>)